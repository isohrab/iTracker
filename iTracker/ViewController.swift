//
//  ViewController.swift
//  iTracker
//
//  Created by sohrab on 22/11/2016.
//  Copyright © 2016 Alireza K. All rights reserved.
//

import UIKit
import AVFoundation
import CoreVideo
import CoreMedia
import CoreGraphics
import CoreImage

class ViewController:   UIViewController, AVCaptureVideoDataOutputSampleBufferDelegate {
    
    
    
    // sliders objects
    
    
    var minDistance:Int32 = 50  // Minimum distance between detected centers
    var parm1:Int32 = 150       // Upper threshold for the internal Canny edge detector
    var parm2:Int32 = 20       // Threshold for center detection
    var minRadius:Int32 = 15    // Minimum radio to be detected. If unknown, put zero as default
    var maxRadius:Int32 = 30    // Maximum radius to be detected. If unknown, put zero as default
 
    
    @IBOutlet weak var infoText: UILabel!
    
    @IBOutlet weak var rightEye: UIImageView!
    @IBOutlet weak var leftEye: UIImageView!
    
    var faceDetector : CIDetector!
    var captureDevice:AVCaptureDevice!
    //var scaleWidth: CGFloat = 1
    //var scaleHeight: CGFloat = 1
    
    // It should be initial only once, because of a lot of overhead
    var context: CGContext!
    
    // For applying filters
    var ciContext: CIContext!
    var filter: CIFilter!
    var ciimage:CIImage!
    
    
    // calibration variables
    var regions:[UIView]!                           // regions for calibration
    var CalibrationActive:Bool = false
    var CalibrationPoints:[[CGPoint]]!            // array of  calibration points! for all 9 point on screen
    var calibrationTimer:Timer!                   // timer to change active (red) region during calibration
    var calibrated:Bool = false                   // show calibration is done
    var activeRegion = 0                          // show active view on calibration
    var xCoefficients:[CGFloat]!                  // Coefficients of x coordinate
    var yCoefficients:[CGFloat]!                  // Coefficients of y coordinate
    var regionPos:[CGPoint]!
    var CalibrationPointCounter:Int = 0           // number of Calib. Point that should be collected!
    var firstTime:Bool = true
    // last corner point
    var firstCorner:CGPoint = CGPoint(x: 0.0, y: 0.0)
    
    var mouse:UIView!
    var noghte:UIView!
    override var prefersStatusBarHidden: Bool {
        return true
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // setup views
        regions = [UIView]()
        regionPos = [CGPoint]()
        let size:CGFloat = 20
        let space = (UIScreen.main.bounds.width - (3*size))/2
        for i in 0...2
        {
            for j in 0...2
            {
                let v = UIView(frame: CGRect(x: CGFloat(j)*(size+space),
                                             y: CGFloat(i)*(size+space),
                                             width: size, height: size))
                regionPos.append(CGPoint(x: CGFloat(j)*(size+space)+size/2, y: CGFloat(i)*(size+space)+size/2))
                v.backgroundColor = UIColor.green
                v.layer.cornerRadius = size/2
                self.view.addSubview(v)
                regions.append(v)
                
            }
            
        }
        // show mouse on the screen
        mouse = UIView(frame: CGRect(x: 150, y: 150, width: 20, height: 20))
        mouse.backgroundColor = UIColor.black
        self.view.addSubview(mouse)
            
        // initial calibration points array
        CalibrationPoints = [[CGPoint]]()
        for _ in 0...8
        {
            let regionPoint = [CGPoint]()
            CalibrationPoints.append(regionPoint)
        }
        // setup corners points
        setupCameraSession()
        
    }
    
    
    @IBAction func Calibrate(_ sender: UIButton) {
        print("@IBAction func Calibrate(_ sender: UIButton)")
        activeRegion = 0;   // -1 = no record!
        CalibrationPointCounter = 0
        CalibrationActive = true
        //calibrationTimer = Timer.scheduledTimer(timeInterval: 4, target: self, selector: #selector(changeRegion), userInfo: nil, repeats: true)
    }
    
    func changeRegion()
    {
        for v in regions{
            v.backgroundColor = UIColor.green
        }
        activeRegion += 1
        CalibrationActive = true
        if activeRegion == 9
        {
            CalibrationActive = false
            calibrationTimer.invalidate()
            processCalibration()
            calibrated = true
        }
        else
        {
            let v = regions[activeRegion]
            v.backgroundColor = UIColor.red
        }
    }
    
    func processCalibration()
    {
        xCoefficients = [CGFloat]()
        yCoefficients = [CGFloat]()
        var screenRegionX = [CGFloat]()
        var screenRegionY = [CGFloat]()
        var eyeMeanX = [CGFloat]()
        var eyeMeanY = [CGFloat]()
        var eyeMeanXY = [CGFloat]()
        var eyeMeanXX = [CGFloat]()
        var eyeMeanYY = [CGFloat]()
        /*var m:[CGPoint] = [CGPoint(x:40 , y:15),
                           CGPoint(x:60 , y:15),
                           CGPoint(x:80 , y:15),
                           CGPoint(x:40 , y:6),
                           CGPoint(x:60 , y:6),
                           CGPoint(x:80 , y:6),
                           CGPoint(x:40 , y:-4),
                           CGPoint(x:60 , y:-4),
                           CGPoint(x:80 , y:-4)]*/
        for i in 0...8
        {
            screenRegionX.append(regionPos[i].x)
            screenRegionY.append(regionPos[i].y)
            
            let m = getMean(points: CalibrationPoints[i])
            eyeMeanX.append(m.x)
            eyeMeanY.append(m.y)
            eyeMeanXY.append(m.x*m.y)
            eyeMeanXX.append(m.x*m.x)
            eyeMeanYY.append(m.y*m.y)
        }
        var xco:[CGFloat] = [0,0,0,0,0,0]
        var yco:[CGFloat] = [0,0,0,0,0,0]
        
        openCVW.getSVD(screenRegionX, withX: eyeMeanX, withY: eyeMeanY, withXY: eyeMeanXY, withXX: eyeMeanXX, withYY: eyeMeanYY, withCo: &xco)
        openCVW.getSVD(screenRegionY, withX: eyeMeanX, withY: eyeMeanY, withXY: eyeMeanXY, withXX: eyeMeanXX, withYY: eyeMeanYY, withCo: &yco)
        for i in 0..<6
        {
            xCoefficients.append(xco[i])
            yCoefficients.append(yco[i])
        }
    }
    
    func gazeToScreen(p: CGPoint) -> CGPoint
    {
        let sx = xCoefficients[0] + xCoefficients[1]*p.x + xCoefficients[2]*p.y + xCoefficients[3]*p.x*p.y + xCoefficients[4]*p.x*p.x + xCoefficients[5]*p.y*p.y
        let sy = yCoefficients[0] + yCoefficients[1]*p.x + yCoefficients[2]*p.y + yCoefficients[3]*p.x*p.y + yCoefficients[4]*p.x*p.x + yCoefficients[5]*p.y*p.y
        return CGPoint(x:sx, y:sy)
    }
    
    func getMean(points: [CGPoint]) -> CGPoint
    {
        var pm = CGPoint(x: 0, y: 0)
        for p in points{
            pm.x += p.x
            pm.y += p.y
        }
        pm.x /= CGFloat(points.count)
        pm.y /= CGFloat(points.count)
        // TODO remove outliers from point and calculate new mean.
        var maxDistance:CGFloat = 0
        var distances = [CGFloat]()
        for p in points
        {
            let d = getDistance(p1: pm, p2: p)
            if maxDistance < d
            {
                maxDistance = d
            }
            distances.append(d)
        }
        
        // remove points over half of the maximum
        var newPoints = [CGPoint]()
        for i in 0..<distances.count
        {
            if distances[i] > (0.3 * maxDistance)
            {
                newPoints.append(points[i])
                
            }
        }
        pm = CGPoint(x:0,y:0)
        for p in newPoints
        {
            pm.x += p.x
            pm.y += p.y
        }
        pm.x /= CGFloat(newPoints.count)
        pm.y /= CGFloat(newPoints.count)
        print("pm=",pm)
        return pm
    }
    
    func getDistance(p1:CGPoint, p2:CGPoint) -> CGFloat
    {
        let d = (p1.x - p2.x)*(p1.x - p2.x) + (p1.y-p2.y)*(p1.y-p2.y)
        return sqrt(d)
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        // VideoImage.layer.addSublayer(previewLayer)
        cameraSession.startRunning()
    }
    
    lazy var cameraSession: AVCaptureSession = {
        let s = AVCaptureSession()
        s.sessionPreset = AVCaptureSessionPresetHigh
        return s
    }()
    
    
    // Setup camera session for captureing the frames
    func setupCameraSession() {
        // Select the front device
        captureDevice = AVCaptureDevice.defaultDevice(withDeviceType: AVCaptureDeviceType.builtInWideAngleCamera, mediaType: AVMediaTypeVideo, position: AVCaptureDevicePosition.front)
        
        do {
            // Get an input from front camera
            let deviceInput = try AVCaptureDeviceInput(device: captureDevice)
            
            // Setup camera session for our configuration
            cameraSession.beginConfiguration()
            
            // Add input to our camera  session
            if (cameraSession.canAddInput(deviceInput) == true) {
                cameraSession.addInput(deviceInput)
            }
            
            // Setup a data output
            let dataOutput = AVCaptureVideoDataOutput()
            
            // Set video format as 420YpCbCr : I choose this ( according to a blog!) because it is good to get monochrome layer
            dataOutput.videoSettings = [(kCVPixelBufferPixelFormatTypeKey as NSString) : NSNumber(value: kCVPixelFormatType_420YpCbCr8BiPlanarFullRange as UInt32)]
            
            // We don't need late frames!
            dataOutput.alwaysDiscardsLateVideoFrames = true
            
            // If session can add new output, let add it :)
            if (cameraSession.canAddOutput(dataOutput) == true) {
                cameraSession.addOutput(dataOutput)
            }
            
            // Commit changes to Cameta session
            cameraSession.commitConfiguration()
            
            // Make a thread
            let queue = DispatchQueue(label: "me.alirezak.iTracker")
            
            // Get frames in another thread!
            dataOutput.setSampleBufferDelegate(self, queue: queue)
            
        }
        catch let error as NSError {
            NSLog("\(error), \(error.localizedDescription)")
        }
    }
    
    // Get image data from sample buffer, it will call automatically after new frame captured
    func captureOutput(_ captureOutput: AVCaptureOutput!, didOutputSampleBuffer sampleBuffer: CMSampleBuffer!, from connection: AVCaptureConnection!) {
        
        // Set the orientation to portrait, default is landscape
        if connection.isVideoOrientationSupported
        {
            connection.videoOrientation = .portrait
        }
        
        // Because it is a selfie!
        if connection.isVideoMirroringSupported
        {
            connection.isVideoMirrored = true
        }
        
        // Get a CMSampleBuffer's Core Video image buffer for the media data
        let imageBuffer =  CMSampleBufferGetImageBuffer(sampleBuffer)
        
        // Lock the base address of the pixel buffer
        CVPixelBufferLockBaseAddress(imageBuffer!, CVPixelBufferLockFlags(rawValue: 0))
        
        // Get the pixel buffer width and height
        let width = CVPixelBufferGetWidthOfPlane(imageBuffer!, 0)
        let height = CVPixelBufferGetHeightOfPlane(imageBuffer!, 0)
        
        // Get the number of bytes per row for the pixel buffer
        let bytesPerRow = CVPixelBufferGetBytesPerRowOfPlane(imageBuffer!, 0)
        
        // Get the Base address of Buffer
        let lumaBuffer = CVPixelBufferGetBaseAddressOfPlane(imageBuffer!, 0)
        
        // Make a Gray space color for our work
        let grayColorSpace = CGColorSpaceCreateDeviceGray()
        
        // Create a bitmap graphics context with the sample buffer data
        context = CGContext(data: lumaBuffer,
                            width: width,
                            height: height,
                            bitsPerComponent: 8,
                            bytesPerRow: bytesPerRow,
                            space: grayColorSpace,
                            bitmapInfo: CGImageAlphaInfo.none.rawValue);
        
        
        let frame = context!.makeImage()
        
        detect(image: frame!)
    }
    
    func captureOutput(_ captureOutput: AVCaptureOutput!, didDrop sampleBuffer: CMSampleBuffer!, from connection: AVCaptureConnection!) {
        // Here you can count how many frames are dopped
    }
    
    
    
    /**
     Get a frame (CGImage) and convert it to CIImage and extract faces, eyes with help of SWIFT function
     
     - Parameters: CGImage (a frame from output buffer)
     - image: a CGImage data
     */
    func detect(image:CGImage)
    {

        // Get CIImage from CGImage
        ciimage = CIImage(cgImage: image)
        
        // Extract faces and eyes from CIImage
        faceDetector = CIDetector(ofType: CIDetectorTypeFace, context: nil, options: [CIDetectorAccuracy:CIDetectorAccuracyHigh])
        
        
        // All faces..
        let faces = faceDetector.features(in: ciimage)
        
        // Loop over all faces and extract eyes and show them on the screen. (we only need first face)
        for face in faces as! [CIFaceFeature] {
            
            var eyeDistance:CGFloat = 200
            if face.hasRightEyePosition && face.hasLeftEyePosition
            {
                eyeDistance = face.rightEyePosition.x - face.leftEyePosition.x
            }
            
            if face.hasLeftEyePosition {
                // In another thread we get image of left eye and show it on the Screen.
                DispatchQueue.main.async {
                    let lEye = self.getEye(i: image, p: face.leftEyePosition, h: CGFloat(image.height), d:eyeDistance)
                    self.leftEye.image = lEye
                }
            }
            
            if face.hasRightEyePosition {
                // In another thread we get image of right eye and show it on the Screen.
                DispatchQueue.main.async {
                    let rEye = self.getEye(i: image, p: face.rightEyePosition, h: CGFloat(image.height), d:eyeDistance)
                    //self.rightEye.image = openCVW.getEdge(rEye, withTreshold: 50, withRatio: 3, withKernel: 3, withBlur: 5)
                    self.rightEye.image = openCVW.getCorner(rEye, fast: 40)
                    
                    
                    // results[0]= circle.x, results[1]= circle.y, results[2]= circle.r,
                    // results[3]= corner.x, results[4]= corner.y
                    var results:[CGFloat] = [0,0,0,0,0]
                    openCVW.getAll(rEye, result: &results)
                    if results[0] == 0 || results[1] == 0 || results[3] == 0 || results[4] == 0
                    {
                        //print("all zero")
                        return
                    }
                    // if corner is NOT in desired range! return
                    if !(results[3] < 50 && results[3] > 25 && results[4] > 30 && results[4] < 50)
                    {
                        //print("no good corner")
                        return
                    }
                    
                    
                    
                     //if it is first time that we assign corner
                    if self.firstTime && false
                    {
                        self.noghte = UIView()
                        self.noghte.layer.backgroundColor = UIColor.white.cgColor
                        self.noghte.frame = CGRect(x: results[2], y: results[3], width: 5, height: 5)
                        self.rightEye.addSubview(self.noghte)
                        self.firstCorner.x = CGFloat(results[3])
                        self.firstCorner.y = CGFloat(results[4])
                        self.firstTime = false
                    }
                    else
                    {
                        // calculate victor from corner to center
                        let gaze = CGPoint(x:results[0] - results[3] * 1000 ,y: results[1] - results[4] * 1000)
                        
                        if self.CalibrationActive
                        {
                            self.CalibrationPoints[self.activeRegion].append(gaze)
                            print("\(gaze.x), \(gaze.y)")
                            self.CalibrationPointCounter += 1
                            if(self.CalibrationPointCounter == 20)
                            {
                                self.CalibrationPointCounter = 0
                                if self.activeRegion == 8
                                {
                                    self.CalibrationActive = false
                                    self.regions[self.activeRegion].backgroundColor = UIColor.green
                                    self.processCalibration()
                                    self.calibrated = true
                                    return
                                }
                                else
                                {
                                    
                                    self.regions[self.activeRegion].backgroundColor = UIColor.green
                                    self.activeRegion += 1
                                    
                                }
                            }
                            self.regions[self.activeRegion].backgroundColor = UIColor.red
                            
                        }
                        else if self.calibrated
                        {
                            let p = self.gazeToScreen(p: gaze)
                            print("gaze point:", p)
                            self.mouse.layer.position = p
                        }
                    }
                }
                
            }
            
        }
        
    }
    
    
    /**
     Get the image, location of Eye, and height and return cropped eye image
     
     - Parameters:
     - i: a CGImage data
     - p: point of Eye in image
     - h: relevant height of eye according to image Height
     
     - Returns: Cropped CGImage
     */
    func getEye(i:CGImage, p:CGPoint, h:CGFloat, d:CGFloat) -> UIImage
    {
        let eyeWidth = 170 * d / 200
        let eyeHeight = 80 * d / 200
        // Make a rectange around the Eye
        let r = CGRect(x: p.x - eyeWidth/2,
                       y: (-1 * p.y) + h - eyeHeight/2,
                       width: eyeWidth ,
                       height: eyeHeight)
        // Crop it!
        let croppedImage = i.cropping(to: r)
        return UIImage(cgImage: croppedImage!)
    }
    
    
    /*func toLineOverlay(inputImage:CIImage) -> UIImage
    {
        /*ciContext = CIContext()
        filter = CIFilter(name: "CILineOverlay")
        filter.setValue(inputImage, forKey: kCIInputImageKey)
        filter.setValue(noise, forKey: "inputNRNoiseLevel")
        filter.setValue(sharpness, forKey: "inputNRSharpness")
        filter.setValue(Intensity, forKey: "inputEdgeIntensity")
        filter.setValue(threshold, forKey: "inputThreshold")
        filter.setValue(contrast, forKey: "inputContrast")
        let filteredImageRef = ciContext.createCGImage(filter.outputImage!, from: (filter.outputImage?.extent)!)
        return UIImage(cgImage: filteredImageRef!)*/
    }*/
    
    
    
    
    
    
    
    
    
    
    
}
