//
//  openCVW.h
//  iTracker
//
//  Created by sohrab on 22/11/2016.
//  Copyright © 2016 Alireza K. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
@interface openCVW : NSObject

// convert an image to grayscale
+(UIImage * ) imageToGray:(UIImage *)image;

// get center of Elipse
+(CGPoint) getElipse:(UIImage *)image;

+(UIImage *) findCircle:(UIImage *)image withMinDist:(int)minDist withParm1:(int)p1 withParm2:(int)p2 withMinR:(int)minR withMaxR:(int)maxR;

+(UIImage *) getEdge:(UIImage *)image withTreshold:(int)t withRatio:(int)r withKernel:(int) k withBlur:(int)b;

+(UIImage *) getCorner:(UIImage *)image fast:(int)f;

+(void)getAll:( UIImage *)image result:(inout CGFloat [5])results;

+(void) getSVD:(const CGFloat [])screen withX: (const CGFloat [])x withY: (const CGFloat [])y withXY: (const CGFloat [])xy withXX: (const CGFloat [])xx withYY: (const CGFloat [])yy withCo:(inout CGFloat [6])co;
@end
NS_ASSUME_NONNULL_END
